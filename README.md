# Kotlin Fundamentals

## Intro

This code follows the Pluralsight "Kotlin Fundameltals" course:

[Kotlin Fundamentals](https://app.pluralsight.com/library/courses/kotlin-fundamentals/table-of-contents)


## Sections

The directories contain list of the sections of the course.

### Section: GettingStarted

The section follows topics like:

- Immutability
- String templates
- Using `if` as an expression
- Handling `null` values
- The `when` statement
- The `try` statement
- Kotlin looping constructs
- Kotlin's support for exceptions

Notes on integration with Java and how Kotlin module is wrapped into Java classes.

InetlliJ IDEA generated directory:

    GettingStarted\out\production\GettingStarted

inside this directory there is a file `MainKt.class` that is an output from `main.kt`. To take a look at the class file we can use the `fernflower` decompiler: [fernflower](https://github.com/fesh0r/fernflower).

Copy `fernwlower` into the home directory of the project and run:

java -jar fernflower.jar GettingStarted\out\production\GettingStarted\MainKt.class decompiled 

Into the _decompiled_ folder there you will find a MainKt.java file.






